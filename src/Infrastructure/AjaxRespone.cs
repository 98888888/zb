﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
	public class AjaxResponeJson
	{
		private const string SuccessMessage = "操作成功";
		private const string FialMessage = "操作失败";
		public bool Success { get; set; }
		public string Message { get; set; }

		public AjaxResponeJson()
		{
			Success = true;
			Message = SuccessMessage;
		}
		public dynamic Data { get; set; }

		public AjaxResponeJson SetFial(string message = FialMessage)
		{
			Success = false;
			Message = message;
			return this;
		}
		public AjaxResponeJson SetSuccess(string message = SuccessMessage)
		{
			Success = true;
			Message = message;
			return this;
		}
		public AjaxResponeJson SetMessage(string message)
		{
			Message = message;
			return this;
		}

		public AjaxResponeJson SetData(dynamic data)
		{
			this.Data = data;
			return this;
		}
	}

	public static class AjaxExtensions
	{
		public static AjaxResponeJson CreateFail(string msg)
		{
			return new AjaxResponeJson().SetFial(msg);
		}
		public static AjaxResponeJson CreateFail() {
			return new AjaxResponeJson().SetFial();
		}
		public static AjaxResponeJson CreateFail(Exception ex)
		{
			return new AjaxResponeJson().SetFial(ex.Message);
		}

		public static AjaxResponeJson CreateSuccess()
		{
			return new AjaxResponeJson().SetSuccess();
		}


	}
}
