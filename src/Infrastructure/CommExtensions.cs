﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Infrastructure
{
    public static class CommExtensions
    {
		public static string GetUserIp(this HttpContext context)
		{
			var ip = context.Request.Headers["X-Forwarded-For"].FirstOrDefault();
			if (string.IsNullOrEmpty(ip))
			{
				ip = context.Connection.RemoteIpAddress.ToString();
			}
			return ip;
		}

		public static bool IsNullOrWhiteSpace(this string str) { return string.IsNullOrWhiteSpace(str); }
	}
}
