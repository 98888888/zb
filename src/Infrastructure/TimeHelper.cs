﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
	public static class TimeHelper
    {
		public static string DateTimeFormat(this DateTime dateTime, DateFormatType dateFormatType,string customer=null)
		{
			switch (dateFormatType)
			{
				case DateFormatType.defalut1:
					return dateTime.ToString("yyyy/MM/dd hh:mm:ss");

				case DateFormatType.default2:
					return dateTime.ToString("yyyy-MM-dd hh:mm:ss");

				case DateFormatType.customer:
					if (customer != null)
					{
						return dateTime.ToString(customer);
					}
					else
					{
						return dateTime.ToString();

					} 
				default:
					return dateTime.ToString();
			}
		}

	}
	public enum DateFormatType {
		/// <summary>
		/// yyyy/MM/dd hh:mm:ss
		/// </summary>
		defalut1,
		/// <summary>
		/// yyyy-MM-dd hh:mm:ss
		/// </summary>
		default2,
		 /// <summary>
		 /// 自定义格式
		 /// </summary>
		customer
	}
}
