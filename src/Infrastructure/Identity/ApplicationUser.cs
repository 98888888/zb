﻿using ApplicationCore.Entities;
using Microsoft.AspNetCore.Identity;
using System;

namespace Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
		 public int RoomId { get; set; }
		 
		/// <summary>
		/// 会员组
		/// </summary>
		 public string Group { get; set; }


		public DateTime CreateAt { get; set; } = DateTime.Now;
		public string Note { get; set; }  
	}
	public class ApplicationRole : IdentityRole
	{

	}
	 
}
