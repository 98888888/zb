﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Infrastructure.Identity
{
    public class AppIdentityDbContextSeed
    {
        public static async Task SeedAsync(UserManager<ApplicationUser> userManager,RoleManager<ApplicationRole> roleManager, AppIdentityDbContext identityContext)
        {
			

			var defaultUser = new ApplicationUser {  UserName = "admin", Email = "a@zb.com" };
            await userManager.CreateAsync(defaultUser, "Aa.111111");
			

			var defultAdminRole = new ApplicationRole { Name = "超级管理员" };
			await roleManager.CreateAsync(defultAdminRole);
			await userManager.AddToRoleAsync(defaultUser, defultAdminRole.Name);


			var defultAgencyRole = new ApplicationRole {   Name="代理商"   };
			await roleManager.CreateAsync(defultAgencyRole);

			var defultMemberRole = new ApplicationRole { Name = "会员" };
			await roleManager.CreateAsync(defultMemberRole);

			var defultGuestRole = new ApplicationRole { Name = "游客" };
			await roleManager.CreateAsync(defultGuestRole);

			var defultDummyRole = new ApplicationRole { Name = "假人" };
			await roleManager.CreateAsync(defultDummyRole);

			var defultWaterArmyRole = new ApplicationRole { Name = "水军" };
			await roleManager.CreateAsync(defultWaterArmyRole);


		}
	}
}
