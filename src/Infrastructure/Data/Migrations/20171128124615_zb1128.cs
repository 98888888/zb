﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Infrastructure.data.Migrations
{
    public partial class zb1128 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChatMsgs_Rooms_RoomId1",
                table: "ChatMsgs");

            migrationBuilder.DropIndex(
                name: "IX_ChatMsgs_RoomId1",
                table: "ChatMsgs");

            migrationBuilder.DropColumn(
                name: "RoomId1",
                table: "ChatMsgs");

            migrationBuilder.AddColumn<string>(
                name: "Doman",
                table: "Rooms",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "ChatMsgs",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Doman",
                table: "Rooms");

            migrationBuilder.AlterColumn<string>(
                name: "Content",
                table: "ChatMsgs",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<int>(
                name: "RoomId1",
                table: "ChatMsgs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ChatMsgs_RoomId1",
                table: "ChatMsgs",
                column: "RoomId1");

            migrationBuilder.AddForeignKey(
                name: "FK_ChatMsgs_Rooms_RoomId1",
                table: "ChatMsgs",
                column: "RoomId1",
                principalTable: "Rooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
