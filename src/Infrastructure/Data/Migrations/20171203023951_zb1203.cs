﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Infrastructure.data.Migrations
{
    public partial class zb1203 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChatMsgs_Rooms_RoomId",
                table: "ChatMsgs");

            migrationBuilder.DropIndex(
                name: "IX_ChatMsgs_RoomId",
                table: "ChatMsgs");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ChatMsgs_RoomId",
                table: "ChatMsgs",
                column: "RoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChatMsgs_Rooms_RoomId",
                table: "ChatMsgs",
                column: "RoomId",
                principalTable: "Rooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
