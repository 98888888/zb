﻿using Microsoft.AspNetCore.Builder;
using ApplicationCore.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class ZBContextSeed
    {
        public static async Task SeedAsync(ZBContext zbContext,
            ILoggerFactory loggerFactory, int? retry = 0)
        {
            int retryForAvailability = retry.Value;
            try
            {
				// TODO: Only run this if using a real database
				// context.Database.Migrate();
				
				await zbContext.MemberGroups.AddAsync(new MemberGroup { Group = "钻石会员" });
				await zbContext.MemberGroups.AddAsync(new MemberGroup { Group = "铜牌会员" });
				var guestRoom = new Room
				{
					Name = "游客房间，不提示注册",
					GuestTimeOut = 1440,
					AMA = AutomaticMessageAudit.Yes,
					RoomNo = "000001"
					  ,
					SpeakInterval = 2,
					State = RoomState.Opened, 

				};
				zbContext.ChatMsgs.Add(new ChatMsg
				{
					Content = "1",
					UserIdFrom = "1",
					 RoomId=guestRoom.Id

				});

				var vipRoom = new Room
				{
					Name = "VIP",
					GuestTimeOut = 0,
					AMA = AutomaticMessageAudit.Yes,
					RoomNo = "000002",
					PassWord = "111",
					SpeakInterval = 1,
					State = RoomState.Opened
				};
				 zbContext.ChatMsgs.Add(new ChatMsg
				{
					Content = "我是vip",
					UserIdFrom = "1",
					RoomId = vipRoom.Id
				});
				  zbContext.Rooms.Add( guestRoom);
				  zbContext.Rooms.Add( vipRoom);
				await zbContext.SaveChangesAsync();
			}
            catch (Exception ex)
            {
                if (retryForAvailability < 10)
                {
                    retryForAvailability++;
                    var log = loggerFactory.CreateLogger<ZBContextSeed>();
                    log.LogError(ex.Message);
                    await SeedAsync(zbContext, loggerFactory, retryForAvailability);
                }
            }
        }

        
    }
}
