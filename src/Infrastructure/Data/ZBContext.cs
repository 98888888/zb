﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ApplicationCore.Entities;


namespace Infrastructure.Data
{

	public class ZBContext : DbContext
	{
		public ZBContext(DbContextOptions<ZBContext> options) : base(options)
		{
		}
		//public CatalogContext()
		//{
		//    // required by migrations
		//}
		public DbSet<ChatMsg> ChatMsgs { get; set; }
		public DbSet<Room> Rooms { get; set; }

		public DbSet<ChatRestriction> ChatRestrictions { get; set; }
		public DbSet<Course> Courses { get; set; }
		public DbSet<Teacher> Teachers { get; set; }
		public DbSet<ShareFile> ShareFiles { get; set; }
		public DbSet<ApplicationUserLoginLog> ApplicationUserLoginLogs { get; set; }

		public DbSet<Menu> Menus { get; set; }
		public DbSet<RelationshipManagement> RelationshipManagements { get; set; }

		public DbSet<MemberGroup> MemberGroups { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<ChatMsg>(ConfigureChatMsg);
			builder.Entity<Room>(ConfigureRoom);
			builder.Entity<MemberGroup>(ConfigureMemberGroup);

		}

		private void ConfigureChatMsg(EntityTypeBuilder<ChatMsg> builder)
		{
			builder.ToTable("ChatMsgs");

			builder.Property(p => p.RoomId)
				.IsRequired(true);

			builder.Property(p => p.Content)
				.IsRequired(true); 

			builder.Property(p => p.UserIdFrom)
				.IsRequired(true);

			builder.Property(p => p.CreateAt)
				.IsRequired(true);

		}

		private void ConfigureMemberGroup(EntityTypeBuilder<MemberGroup> builder)
		{
			builder.ToTable("MemberGroups");
			builder.Property(p => p.Group)
				.IsRequired(true);

			builder.HasIndex(p => p.Group)
				.IsUnique(true); 

		}

		private void ConfigureRoom(EntityTypeBuilder<Room> builder)
		{
			builder.ToTable("Rooms");


			builder.Property(ci => ci.Name)
				.IsRequired(true)
				.HasMaxLength(50);

			builder.HasIndex(i => i.RoomNo).IsUnique(true);

			builder.Property(i => i.RoomNo)
				.IsRequired(true)
				.HasMaxLength(6);
			 

		}


	}
}
