﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.AdminMvc.Models
{
	public class RoomViewModel
	{
		 
		public int Id { get; set; }
		 
		 
		public string Name { get; set; } 

		public string RoomNo { get; set; }

		public string Note1 { get; set; }
		public string Note2 { get; set; }
		public string Note3 { get; set; }
		public string Logo { get; set; }
		public string Remarks { get; set; }
		public int GuestTimeOut { get; set; }
		public RoomState State { get; set; }
			

		public RoomViewModel()
		{

		}
		public RoomViewModel(Room room) 
		{
			Id = room.Id;
			Name = room.Name;
			RoomNo = room.RoomNo;
			this.Note1 = room.Note1;
			this.Note2 = room.Note2;
			this.Note3 = room.Note3;
			this.Logo = room.Logo;
			this.GuestTimeOut = room.GuestTimeOut;
			Remarks = room.Remarks;
			this.State = room.State;
		}
	}
}
