﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.AdminMvc.Models
{
	public class Pager {
		public int limit { get; set; }
		public int page { get; set; }
	}
	public class TableViewModel
	{
		/// <summary>
		/// 
		/// </summary>
		public int code { get; set; } = 0;
		/// <summary>
		/// 
		/// </summary>
		public string msg { get; set; } = "";
		/// <summary>
		/// 
		/// </summary>
		public int count { get; set; } = 0;
		/// <summary>
		/// 
		/// </summary>
		public object data { get; set; }
	}


	public static class TableViewModelExt {
		public static JsonResult CreateJsonResult(this TableViewModel table  ) {
			return new JsonResult(table);
		}
	}
}
