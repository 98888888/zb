﻿/**
  项目JS主入口
  以依赖layui的layer和form模块为例
**/
layui.define(['layer', 'form', 'element', 'layedit', 'laydate'], function (exports) {
	var layer = layui.layer
		, form = layui.form;
	var layer = layui.layer
		, layedit = layui.layedit
		, laydate = layui.laydate;

	var $ = layui.jquery
		, element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块

	var postUrl = "/room/create";

	//日期
	laydate.render({
		elem: '#DateTimeStart'
	});
	laydate.render({
		elem: '#DateTimeEnd'
	});


	//自定义验证规则
	form.verify({
		title: function (value) {
			if (value.length < 5) {
				return '标题至少得5个字符啊';
			}
		}
		, pass: [/(.+){6,12}$/, '密码必须6到12位']
		, content: function (value) {
			layedit.sync(editIndex);
		}
	});


	//监听提交
	form.on('submit(demo1)', function (data) {

		$.post(postUrl, data.field, function (rsp) {


			layer.msg(rsp.message);

		});

		return false;
	});


	exports('index', {}); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});    