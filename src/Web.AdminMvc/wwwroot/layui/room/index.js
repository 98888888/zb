﻿/**
  项目JS主入口
  以依赖layui的layer和form模块为例
**/
layui.define(['laydate', 'laypage', 'layer', 'table', 'carousel', 'upload', 'element', 'jquery'], function (exports) {
	var laydate = layui.laydate //日期
		, laypage = layui.laypage //分页
	layer = layui.layer //弹层
		, table = layui.table //表格
		, carousel = layui.carousel //轮播
		, upload = layui.upload //上传
		, element = layui.element; //元素操作

	var $ = layui.$;

	//url
	var baseUrl = "/Room/";
	var delUrl = baseUrl + "Del";
	var addUrl = baseUrl + "Create";
	var getUrl = baseUrl + "GetPageList";
	var stopUrl = baseUrl + "stop";

	//执行一个 table 实例
	var tableIns=table.render({
		elem: '#list'
		, url: getUrl //数据接口
		, page: true //开启分页
		, cols: [[ //表头
			//{ field: 'id', title: 'ID', width: 80, sort: true, fixed: 'left' }
			{ field: 'name', title: '名称', width: 180 }
			, { field: 'roomNo', title: '房间号', width: 130, sort: true }
			, { field: 'state', title: '状态', width: 100 }
			, { field: 'note1', title: '公告', width: 100 }
			, { field: 'note2', title: '版权说明', width: 100 }
			, { field: 'guestTimeOut', title: '游客时间', width: 100 }

			, { fixed: 'right', width: 165, align: 'center', toolbar: '#barDemo' }
		]]
	});

	//分页
	laypage.render({
		elem: 'pageDemo' //分页容器的id
		, count: 100
		, skin: '#1E9FFF' //自定义选中色值
		//,skip: true //开启跳页
		, jump: function (obj, first) {
			if (!first) {
				layer.msg('第' + obj.curr + '页');
			}
		}
	});



	//监听工具条
	table.on('tool(demo)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
		var data = obj.data; //获得当前行数据

		var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
		var tr = obj.tr; //获得当前行 tr 的DOM对象

		if (layEvent === 'stop') { //停用
			$.post(stopUrl, { "id": data.id }, function (rsp) {
				 
				layer.msg(rsp.message);
				reload();
			});
		} else if (layEvent === 'del') { //删除


			layer.confirm('真的删除行么', function (index) {

				$.post(delUrl, { "id": data.id }, function (rsp) {
					layer.msg(rsp.message);
					if (rsp.success) {
						obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
						layer.close(index);

					}
					else {


					}

				});


				//向服务端发送删除指令
			});
		} else if (layEvent === 'edit') { //编辑
			open(data.id, "编辑房间");
		}
	});
	var reload = function () {
		 

		//这里以搜索为例
		tableIns.reload({
			where: { //设定异步数据接口的额外参数，任意设
				aaaaaa: 'xxx'
				, bbb: 'yyy'
				//…
			}
			, page: {
				curr: 1 //重新从第 1 页开始
			}
		});

	}
	var open = function (id, title) {

		var that = this;
		//多窗口模式，层叠置顶
		layer.open({
			type: 2 //此处以iframe举例
			, title: title
			, area: ['600px', '550px']
			, shade: 0.3
			, maxmin: true
			, content: addUrl + "/" + id
			, zIndex: layer.zIndex //重点1
			, success: function (layero) {
				layer.setTop(layero); //重点2
			}
		});

	}

	var $ = layui.$, active = {
		getQuery: function () { //查询

		}
		, add: function () { //添加
			open(0, "添加房间");
		}
	};

	$('.demoTable .layui-btn').on('click', function () {
		var type = $(this).data('type');
		active[type] ? active[type].call(this) : '';
	});


	exports('index', {}); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});    