﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ApplicationCore.Interfaces;
using ApplicationCore.Entities;

namespace Web.Admin.Pages.RoomPage
{
    public class IndexModel : PageModel
    {
		private readonly IRoomRepository  _repo;
		

		public IndexModel(IRoomRepository repo) {
			_repo = repo;
		}


		public List<ApplicationCore.Entities.Room> GetList { get; set; } = new List<ApplicationCore.Entities.Room>();

		public void OnGet()
        {
			this.GetList = _repo.ListAll().ToList();


			 
        }
    }
}
