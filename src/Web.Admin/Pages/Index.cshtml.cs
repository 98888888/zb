﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ApplicationCore.Interfaces;
using ApplicationCore.Entities;

namespace Web.Admin.Pages
{
    public class IndexModel : PageModel
    {
		private readonly IRepository<Menu> _menuRepo;

		public IndexModel(IRepository<Menu> menuRepo) {
			_menuRepo = menuRepo;
		}

		public void OnGet()
        {

			 
        }
    }
}
