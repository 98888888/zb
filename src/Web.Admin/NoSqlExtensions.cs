﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Admin
{
	/// <summary>
	/// nosql服务扩展
	/// </summary>
	public static class NoSqlExtensions
	{
		/*

		/// <summary>
		/// 使用Redis
		/// </summary>
		/// <param name="services"></param>
		/// <param name="options"></param>
		/// <returns></returns>
		public static IServiceCollection UseRedis(
		   this IServiceCollection services,
		   Action<RedisConfig> options = null)
		{
			RedisConfig option = new RedisConfig();
			options?.Invoke(option);
			ObjectMapper.MapperTo<RedisConfig>(option, ConfigFileHelper.Get<RedisConfig>());//优先级装饰器
			services.AddSingleton(option);
			services.AddSingleton<RedisManager, RedisManager>();
			return services;
		}

		/// <summary>
		/// 使用Mongodb
		/// </summary>
		/// <param name="services"></param>
		/// <param name="options"></param>
		/// <returns></returns>
		public static IServiceCollection UseMongodb(
		  this IServiceCollection services,
		  Action<MongodbConfig> options = null)
		{
			MongodbConfig option = new MongodbConfig();
			options?.Invoke(option);
			ObjectMapper.MapperTo<MongodbConfig>(option, ConfigFileHelper.Get<MongodbConfig>());//优先级装饰器
			services.AddSingleton(option);
			services.AddSingleton<MongodbManager, MongodbManager>();
			return services;
		}
		*/
	}
}
