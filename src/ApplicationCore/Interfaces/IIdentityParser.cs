﻿using System.Security.Principal;

namespace ApplicationCore.Interfaces
{
    public interface IIdentityParser<T>
    {
        T Parse(IPrincipal principal);
    }
	public class Principal : IPrincipal
	{
		public IIdentity Identity { get; set; }

		public bool IsInRole(string role)
		{
			throw new System.NotImplementedException();
		}
	}
}
