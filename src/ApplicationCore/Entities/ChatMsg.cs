﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
	public class ChatMsg:BaseEntity
	{
		public int RoomId { get; set; }  

		public string Content { get; set; }
		public string UserIdFrom { get; set; }
		 
		public string UserIdTo { get; set; }
		public ChatMsgState ChatMsgState { get; set; } = ChatMsgState.Pass;
		public ChatMsgType ChatMsgType { get; set; } = ChatMsgType.Normal;
		public DateTime CreateAt { get; set; } = DateTime.Now;
		public DateTime UpdateAt { get; set; }  

		public string Other { get; set; }

		public ChatMsg() { }

		public ChatMsg(string userid,int roomid,string content):this() {
			this.UserIdFrom = userid;
			this.RoomId = roomid;
			this.Content = content;
		}


	}

	public enum ChatMsgState
	{
		/// <summary>
		/// 通过
		/// </summary>
		Pass,
		/// <summary>
		/// 不通过
		/// </summary>
		NoPass,
		/// <summary>
		/// 待审
		/// </summary>
		WaitforAudit,
		/// <summary>
		/// 删除的
		/// </summary>
		Delete
	}
	public enum ChatMsgType
	{
		/// <summary>
		/// 正常聊天
		/// </summary>
		Normal,
		/// <summary>
		/// 私聊
		/// </summary>
		PrivateChat,
		/// <summary>
		/// 其他
		/// </summary>
		Other



	}
}
