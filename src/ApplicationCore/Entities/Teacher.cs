﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Teacher : BaseEntity
	{
		public string Name { get; set; }

		public DateTime CreateAt { get; set; }
	}
}
