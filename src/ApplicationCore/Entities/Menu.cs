﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
	public  class Menu:BaseEntity
    {
		public string Name { get; set; }
		public string Url { get; set; }
		public int Pid { get; set; }
		public string Other { get; set; }
	}

	/// <summary>
	/// 对象管理  各种关联
	/// </summary>
	public class RelationshipManagement  : BaseEntity
	{
		public RelationshipmanagementType  RMType { get; set; }
		public string KeyId { get; set; }
		public string KeyId2 { get; set; } 
		public string Other { get; set; }

		public string Note { get; set; }
	}

	public enum RelationshipmanagementType
	{
		RoleMenu
	}
}
