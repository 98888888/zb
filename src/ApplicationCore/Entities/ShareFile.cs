﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class ShareFile : BaseEntity
	{
		public string Name { get; set; }
		public string Path { get; set; }
		
	}
}
