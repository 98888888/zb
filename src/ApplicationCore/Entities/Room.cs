﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text; 

namespace ApplicationCore.Entities
{
	 
    public class Room:BaseEntity
    {  
		[DisplayName("yyID号")]
		public string YYLiveId { get; set; }
		[DisplayName("斗鱼ID号")]
		public string DYLiveId { get; set; }
		[DisplayName("直播地址")]
		public string  LiveUrl { get; set; }
		[DisplayName("房间号")]
		public string RoomNo { get; set; }
		[DisplayName("房间")]
		public string Name { get; set; }
		[DisplayName("密码")]
		public string PassWord { get; set; }
		[DisplayName("公告")]
		public string Note1 { get; set; }
		[DisplayName("版权说明")]
		public string Note2 { get; set; }
		[DisplayName("权限说明")]
		public string Note3 { get; set; }

		[DisplayName("备注")]
		public string Remarks { get; set; }
		[DisplayName("在线人数")]
		public int OnlineUsers { get; set; } = 0;

		/// <summary>
		/// 房间logo
		/// </summary>
		[DisplayName("房间logo")]
		public string Logo { get; set; } = "/images/logo.png";

		/// <summary>
		/// 消息自动审核
		/// </summary>
		[DisplayName("消息自动审核")]
		public AutomaticMessageAudit AMA { get; set; } = AutomaticMessageAudit.Yes;

		/// <summary>
		/// 游客时间 填写1440，表示不提示注册；填写0，表示需要立即注册
		/// </summary>
		[DisplayName("游客时间")]
		public int GuestTimeOut { get; set; } = 1440;

		/// <summary>
		/// 直播间发言的频率，单位：秒（用以防止用户刷屏，设置为0，则不限制发言频率）
		/// </summary>
		[DisplayName("发言频率")]
		public int SpeakInterval { get; set; } = 5;

		/// <summary>
		/// 客服qq 
		/// </summary>
		[DisplayName("客服qq")]
		public string ServiceQQ { get; set; }
		/// <summary>
		/// 滚动公告
		/// </summary>
		[DisplayName("滚动公告")]
		public string Scroll2Notice { get; set; }

		[DisplayName("状态")]
		public RoomState State { get; set; } = RoomState.Opened;
		[DisplayName("域名")]
		public string Doman { get; set; }
		[DisplayName("开始时间")]
		public DateTime? DateTimeStart { get; set; } = DateTime.Now;

		[DisplayName("结束时间")]
		public DateTime? DateTimeEnd { get; set; } = DateTime.Now.AddDays(10);

		 

	}

	public enum RoomState { 
		Opened, Shutdown
	}
	public enum AutomaticMessageAudit { Yes,Not}
}
