﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
	/// <summary>
	/// ip限制
	/// </summary>
	public class ChatRestriction : BaseEntity
	{
		public string IP { get; set; }
		public RestrictionsType RestrictionsType { get; set; }
		public string OptUserId { get; set; }
		public string Note { get; set; }
		public DateTime CreateAt { get; set; }
	}
	/// <summary>
	/// 限制类型
	/// </summary>
	public enum RestrictionBy
	{
		IP, UserId
	}
	/// <summary>
	/// 限制程度
	/// </summary>
	public enum RestrictionsType
	{
		/// <summary>
		/// 只能看
		/// </summary>
		ChatReadOnly,
		/// <summary>
		/// 不允许发言
		/// </summary>
		ChatNotAllowed,

		/// <summary>
		/// 禁止登陆
		/// </summary>
		NotAllowedLogin
	}
}
