﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
	/// <summary>
	/// 会员登录日志
	/// </summary>
	public class ApplicationUserLoginLog : BaseEntity
	{
		public string UserId { get; set; }
		public string IP { get; set; }
		public DateTime CreateAt { get; set; }
		public string Other { get; set; }
		public OptType OptType { get; set; }
		public LogSource LogSource { get; set; }

	public ApplicationUserLoginLog() {

			this.CreateAt = DateTime.Now;
		}

	}
	public enum LogSource { Middleware, Attribute,Code,Other }
	public enum OptType { SignIn, SignOut, ChatJoined,ChatLeft }
}
