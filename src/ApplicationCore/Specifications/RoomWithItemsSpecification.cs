﻿using ApplicationCore.Entities;
using System;
using System.Linq;

namespace ApplicationCore.Specifications
{
	public class RoomSpecifications : BaseSpecification<Room>
	{
		public RoomSpecifications(string no, string name,string doman) :
			base(i=>(string.IsNullOrWhiteSpace(name)||i.Name== name)
			&& (string.IsNullOrWhiteSpace(no) || i.RoomNo == no)
			&& string.IsNullOrWhiteSpace(doman)||i.Doman== doman)
		{ }

		public RoomSpecifications(string no ) :
			base(i => i.RoomNo == no)
		{ }
	}


	public class ChatMsgSpecifications : BaseSpecification<ChatMsg>
	{
		public ChatMsgSpecifications(int roomId, DateTime day) :
			base(i => i.RoomId == roomId
			&& ( (i.CreateAt >= new DateTime(day.Year, day.Month, day.Day)&&
			i.CreateAt <= DateTime.Now
			))
			 )
		{ }

		 
	}
}
