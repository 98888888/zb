﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System;

namespace ApplicationCore.Specifications
{
	public class ChatMsgWithItemsSpecification : BaseSpecification<ChatMsg>
	{
		public ChatMsgWithItemsSpecification(int? roomId,DateTime? start ,DateTime? end)
            : base(i => (!roomId.HasValue || i.RoomId==roomId) &&
                (!start.HasValue || i.CreateAt >start  )&& (!end.HasValue || i.CreateAt < end))
        {
			 
		}


	}
}
