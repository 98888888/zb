﻿using ApplicationCore.Interfaces;
using Web;

namespace ApplicationCore.Services
{
    public class UriComposer : IUriComposer
    {
        private readonly ZBSettings  _zBSettings;

        public UriComposer(ZBSettings catalogSettings) => _zBSettings = catalogSettings;

        public string ComposePicUri(string uriTemplate)
        {
            return uriTemplate.Replace("http://catalogbaseurltobereplaced", _zBSettings.ZBBaseUrl);
        }
    }
}
