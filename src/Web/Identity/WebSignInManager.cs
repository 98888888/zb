﻿using Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Web.Identity
{
	public class WebSignInManager : SignInManager<ApplicationUser>
	{
		public WebSignInManager(UserManager<ApplicationUser> userManager, 
			IHttpContextAccessor contextAccessor, 
			IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory, 
			IOptions<IdentityOptions> optionsAccessor,
			ILogger<SignInManager<ApplicationUser>> logger, 
			IAuthenticationSchemeProvider schemes) 
			: base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes)
		{
		}

		public override Task<SignInResult> PasswordSignInAsync(ApplicationUser user, string password, bool isPersistent, bool lockoutOnFailure)
		{
			return base.PasswordSignInAsync(user, password, isPersistent, lockoutOnFailure);
		}
	}
}
