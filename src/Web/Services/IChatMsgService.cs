﻿using ApplicationCore.Specifications;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Web.ViewModels;

namespace Web.Services
{
	public interface IChatMsgService
	{
		Task<List<ChatMsgViewModel>> GetItemsByRoomNo(string no, DateTime day);


		Task<List<ChatMsgViewModel>> GetItemsByRoomId(int id, DateTime day);

		Task<ChatMsgViewModel> SendChatMsg(string message, int roomId,string fromUserId, string toUserId);
	}
}