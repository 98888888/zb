﻿using ApplicationCore.Interfaces;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using System.Linq;
using Web.ViewModels;
using System.Collections.Generic;
using ApplicationCore.Specifications;
using System;

namespace Web.Services
{
	public class RoomService : IRoomService
	{
		private readonly IRoomRepository _roomRepo;


		public RoomService(IRoomRepository roomRepo)
		{
			this._roomRepo = roomRepo;

		}
		public Task<RoomViewModel> Get(int roomId)
		{
			var room = _roomRepo.GetById(roomId);
			if (room == null)
				return null;
			return Task.FromResult(new RoomViewModel(room));

		}


		public Task<RoomViewModel> Get(string no)
		{
			var re = _roomRepo.List(new RoomSpecifications(no));
			if (re == null) return null;

			var room = re.SingleOrDefault();
			if(room==null) return null;

			return Task.FromResult(new RoomViewModel(room));
		}

	}
}
