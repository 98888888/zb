﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Web.ViewModels;

namespace Web.Hubs
{
	public interface IUserTracker<out THub>
	{
		Task<IEnumerable<UserDetails>> UsersOnline();
		Task AddUser(HubConnectionContext connection, UserDetails userDetails);
		Task RemoveUser(HubConnectionContext connection);

		event Action<UserDetails[]> UsersJoined;
		event Action<UserDetails[]> UsersLeft;
	}
}
