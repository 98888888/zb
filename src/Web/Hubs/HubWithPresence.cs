﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Web.ViewModels;

namespace Web.Hubs
{
	public class HubWithPresence : Hub
	{
		private IUserTracker<HubWithPresence> _userTracker;

		public HubWithPresence(IUserTracker<HubWithPresence> userTracker)
		{
			_userTracker = userTracker;
		}

		public Task<IEnumerable<UserDetails>> GetUsersOnline()
		{
			return _userTracker.UsersOnline();
		}

		public virtual Task OnUsersJoined(UserDetails[] user)
		{
			 
			return Task.CompletedTask;
		}

		public virtual Task OnUsersLeft(UserDetails[] user)
		{
			return Task.CompletedTask;
		}
	}
}
