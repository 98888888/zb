﻿window.console = window.console || (function () {
    return null;
    var url = window.location.href;
    if (url.indexOf('live.shy198.com') > -1)
    {
        if (getUrlParam("debug") != '1') {
            return "";
        }
    }
    
    var c = {}; c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile
	= c.clear = c.exception = c.trace = c.assert = function () { };
    return c;
})();

//$msg = {

//    var showmsg = function (title, msgcontent) {
//        $.messager.show({
//            title: title,
//            msg: msgcontent,
//            showType: 'show'
//        });
//    };
// }
function addTab(subtitle, url, icon) {
    if (!$("#mainTab").tabs('exists', subtitle)) {
        $("#mainTab").tabs('add', {
            title: subtitle,
            content: createFrame(url),
            closable: true,
            icon: icon
        });
    } else {
        $("#mainTab").tabs('select', subtitle);
        $("#tab_menu-tabrefresh").trigger("click");
    }
    //$(".layout-button-left").trigger("click");
    //tabClose();
}
function createFrame(url) {
    var s = '<iframe frameborder="0" src="' + url + '" scrolling="auto" style="width:100%; height:99%"></iframe>';
    return s;
}



//获取url中的参数
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
//调用： var time1 = new Date().Format("yyyy-MM-dd");var time2 = new Date().Format("yyyy-MM-dd HH:mm:ss");
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

function getNowDate1() {
  return new Date().Format("yyyy-MM-dd hh:mm");
}
function getNowDate() {
    return new Date().Format("yyyy-MM-dd hh:mm:ss");
}
function getNowDate2() {
    return new Date().Format("yyyy/MM/dd hh:mm:ss");
}
function getNowDateWithTime() {
    return new Date().Format("hh:mm");
}
function NewDateId() {
    var date = new Date();
    var seperator1 = "-";
    var seperator2 = ":";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var newId = date.getFullYear() + "" + month + "" + strDate
            + date.getHours() +  date.getMinutes()
            + date.getSeconds() + GetRandomNum(100000,999999);
    return newId;
}
function GetRandomNum(Min, Max) {
    var Range = Max - Min;
    var Rand = Math.random();
    return (Min + Math.round(Rand * Range));
}
 
function GetGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });

}

function winModalFullScreen(strURL) {
    var sheight = screen.height - 70;
    var swidth = screen.width - 10;
    var winoption = "dialogHeight:" + sheight + "px;dialogWidth:" + swidth + "px;status:yes;scroll:yes;resizable:yes;center:yes";

    var tmp = window.showModalDialog(strURL, window, winoption);
    return tmp;
}
function winOpenFullScreen(strURL) {
    window.open(strURL, '新窗口', 'width=' + (window.screen.availWidth - 10) + ',height=' + (window.screen.availHeight - 50) + ',top=0,left=0,location=no ,resizable=no,status=yes,menubar=no,scrollbars=yes');
    //var sheight = screen.height - 70;
    //var swidth = screen.width - 10;
    //var winoption = "left=0,top=0,height=" + sheight + ",width=" + swidth + ",toolbar=yes,menubar=yes,location=yes,status=yes,scrollbars=yes,resizable=yes";

    //var tmp = window.open(strURL, '', winoption);
    //return tmp;
}


function nowSeconds() {
    var dateTime = new Date();
    var hh = dateTime.getHours();
    var mm = dateTime.getMinutes();
    var ss = dateTime.getSeconds();
    return hh * 3600 + mm * 60 + ss;
}

function GetClientDeviceType() {
    if (/AppleWebKit.*Mobile/i.test(navigator.userAgent) || (/MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE/.test(navigator.userAgent))) {
        if (window.location.href.indexOf("?mobile") < 0) {
            return 'Mobile';
            try {
                if (/Android|webOS|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) { 				// 判断访问环境是 Android|webOS|iPad|iPod|BlackBerry 则加载以下样式 				
                    
                }
                else if (/iPhone/i.test(navigator.userAgent)) { 				// 判断访问环境是 iPhone 则加载以下样式 				

                }
                else { 				// 判断访问环境是 其他移动设备 则加载以下样式 				

                }
            }
            catch (e) {

            }
        }
    }
    else {  
        return 'PC';
    }  
}
function IsMobileDevice() {
    if (/AppleWebKit.*Mobile/i.test(navigator.userAgent) || (/MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE/.test(navigator.userAgent))) {
        if (window.location.href.indexOf("?mobile") < 0) {
            return true;
        }
    }
    else {
        return false;
    }
}

//设置账户中间部分为星号
function SetStart(str) {
    var len = str.length;
    if (len > 4)
    {
       
        var rpStr = str.substring(2, len - 2);

        return str.replace(rpStr, '****');

    }
    else if (len > 2) {
        var rpStr = str.substring(0, len - 1);

        return str.replace(rpStr, '****');
    }
    else { 
        return '****';
    }

   

}