﻿using Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class UserViewModel
    {
		public string UserName { get; set; }
		public int RoomId { get; set; }

		public List<string> RoleName { get; set; }

		public UserViewModel( )
		{ 

		}


		public UserViewModel(ApplicationUser user) {
			this.UserName = user.UserName;
			this.RoomId = user.RoomId;
			 
		}
	}
}
