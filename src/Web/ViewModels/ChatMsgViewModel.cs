﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Entities;

namespace Web.ViewModels
{
    public class ChatMsgViewModel
	{
		public object Id { get; set; }
		[Required]
		public string Content { get; set; }

		public int RoomId { get; set; }

		public string UserIdFrom { get; set; }
		public string UserIdTo { get; set; }
		public DateTime CreateAt { get; set; }

		public ChatMsgType ChatMsgType { get; set; }

		public ChatMsgViewModel() { }

		public ChatMsgViewModel(ChatMsg msg):this() {
			this.Content = msg.Content;
			this.ChatMsgType = msg.ChatMsgType;
			this.RoomId = msg.RoomId;
			this.UserIdFrom = msg.UserIdFrom;
			this.UserIdTo = msg.UserIdTo;
			this.CreateAt = msg.CreateAt;
			this.Id = msg.Id;
			
		}
	}

	public class UserDetails
	{
		public UserDetails(string connectionId, string name,int roomid)
		{
			ConnectionId = connectionId;
			Name = name;
			this.RoomId = roomid;
		}

		public string ConnectionId { get; }
		public string Name { get; }

		public int RoomId { get; }

	}
}
