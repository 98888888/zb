﻿using Web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Web.Controllers.Api
{
    [Route("api/[controller]/[action]")]
    public class BaseApiController : Controller
    { }
}
