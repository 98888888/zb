﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Web.Services;
using Xunit;
using System.Linq;
using ApplicationCore.Specifications;

namespace IntegrationTests 
{
	public class RoomServiceTest
	{
		private readonly ZBContext _zbContext;
		private IRoomRepository _roomRepo;
		private RoomService _service;

		private IChatMsgRepository _chatMsgRepo;
		private ChatMsgService _chatMsgService;

		public RoomServiceTest()
		{


			var dbOptions = new DbContextOptionsBuilder<ZBContext>()
			   .UseInMemoryDatabase(databaseName: "zb")
			   .Options;
			_zbContext = new ZBContext(dbOptions);

			_roomRepo = new RoomRepository(_zbContext);
			_service = new Web.Services.RoomService(_roomRepo);

			_chatMsgRepo= new ChatMsgRepository(_zbContext);
			_chatMsgService = new ChatMsgService(_chatMsgRepo,_roomRepo, _zbContext);

			BaseData();

		}
		

		public async void BaseData()
		{
			var guestRoom = new Room
			{
				Name = "游客房间，不提示注册",
				GuestTimeOut = 1440,
				AMA = AutomaticMessageAudit.Yes,
				RoomNo = "000001"
					  ,
				SpeakInterval = 2,
				State = RoomState.Opened,

			};
			

			var vipRoom = new Room
			{
				Name = "VIP",
				GuestTimeOut = 0,
				AMA = AutomaticMessageAudit.Yes,
				RoomNo = "000002",
				PassWord = "111",
				SpeakInterval = 1,
				State = RoomState.Opened
			};
			
			_zbContext.Rooms.Add(guestRoom);
			_zbContext.Rooms.Add(vipRoom);
			_zbContext.SaveChanges();


			_zbContext.ChatMsgs.Add(new ChatMsg
			{
				Content = "1",
				UserIdFrom = "1",
				RoomId = _zbContext.Rooms.First(s=>s.RoomNo=="000001").Id

			});
			_zbContext.ChatMsgs.Add(new ChatMsg
			{
				Content = "我是vip",
				UserIdFrom = "1",
				RoomId = _zbContext.Rooms.First(s => s.RoomNo == "000002").Id
			});
			_zbContext.SaveChanges();
		}

		[Fact]
		public async void GetList()
		{
			Assert.NotNull(_zbContext.Rooms.AnyAsync());

		}

		[Fact]
		public async void GetByNo()
		{

			var newRoom = await _service.Get("000001");


			Assert.NotNull(newRoom);


		}

		[Fact]
		public async void GetChatMsgList()
		{
			var start = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
			var room = _roomRepo.List(new RoomSpecifications("000001")).SingleOrDefault();
			var list = _zbContext.ChatMsgs .ToList();
			var list2 = _zbContext.ChatMsgs.Where(s=>   s.RoomId == room.Id).ToList();

			Assert.True(_zbContext.ChatMsgs.Any(s=>s.RoomId== room.Id
			 ), "_zbContext"); 

			var re =await _chatMsgService.GetItemsByRoomId(1, DateTime.Now);

			Assert.True(re.Any(), "_chatMsgRepo");

		}

		[Fact]
		public async void SendChatMsg()
		{
			var newRoom = await _service.Get("000001");


			var re = await _chatMsgService.SendChatMsg("test", newRoom.Id, "test", null);

			Assert.NotNull(re);

		}
	}
}
