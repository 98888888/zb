﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace codesmith
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();


		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (this.checkBox1.Checked)
			{
				var html= File.ReadAllText("v.html");
				var name = this.listBox1.SelectedItem + "";
				var type = _allEntity.First(s=>s.Name==name);

				var code = new  StringBuilder();
				foreach (var item in type.GetProperties())
				{
					var temp = "";
					var attr = item.GetCustomAttribute<DisplayNameAttribute>();
					
					var label = attr==null?item.Name:attr.DisplayName;
					temp = html.Replace("{label}", label);

					temp = temp.Replace("{name}", item.Name);

					temp = temp.Replace("{value}", "@Model."+ item.Name);

					code.AppendLine(temp);
				}


				SaveFile(CodeType.View,   name, code.ToString());
			}
		}
		private enum CodeType {
			 View,C
		}
		void SaveFile(CodeType ct,string name, string code)
		{
			try
			{
				System.IO.Directory.CreateDirectory("view");
				System.IO.Directory.CreateDirectory("c");
			}
			catch  
			{

				 
			}
			switch (ct)
			{
				case CodeType.View:
					System.IO.File.WriteAllText(@"view\"+ name+".chml", code);
					break;
				case CodeType.C:
					System.IO.File.WriteAllText(@"c\" + name + ".cs", code);
					break;
				default:
					break;
			}
			
		}
		private List<Type> _allEntity;
		private void Form1_Load(object sender, EventArgs e)
		{
			_allEntity = new List<Type>();
			   var entitys = Assembly.Load("ApplicationCore");

			Type[] publicTypes = entitys.GetExportedTypes();


			foreach (Type item in publicTypes.Where(s => s.Namespace == "ApplicationCore.Entities") )
			{
				if (item.Name == "Room")
				{

				}

				_allEntity.Add(item);
				this.listBox1.Items.Add(item.Name);

			}

			int a = 1;
		}
	}
}
